/* Before anything: I'm not a javascript, node or toolbox wizard, so I've
   done my best to prevent this mod from doing any kind of dumb shit. Enjoy!
   In case you get banned: don't come crying to me and don't bother toolbox devs either.
*/

module.exports = function GuildQuestUtils(mod) {
    let supportedVersion = [92, 100, 115],
        failsafe = false,
        isNewbie = false,
        isGuildMaster = false,
        questCount = 0,
        guildLevel,
        weeklyCap,
        bossName,
        bossArea,
        activeQuests = [],
        blockedQuests = []

    const rallyQuest = [10005, 10006, 10007, 10008, 10009, 10010]

    /* avoid crashes due to query fuckup - don't remember where i found this */
    /*process.on('unhandledRejection', (error, promise) => {
      console.log(`Prevented toolbox from crashing due to ${error}`);
      console.log(`Culprit: ${promise}`);
      });*/

    // Safety check for game updates
    if (!supportedVersion.includes(mod.majorPatchVersion)) {
        mod.error(`Mod does not support patch ${mod.majorPatchVersion} and automatic begin/completion of quests have been disabled for your safety.`)
        mod.error(`However, normal functions such as the gbam and level up notifier will remain working.`)
        failsafe = true
    }

    // Definitions
    // (might be redundant but still including them just in case)
    mod.dispatch.addDefinition('C_REQUEST_START_GUILD_QUEST', 1, [['questId', 'int32']])
    mod.dispatch.addDefinition('C_REQUEST_CANCEL_GUILD_QUEST', 1, [['questId', 'int32']])
    mod.dispatch.addDefinition('C_REQUEST_FINISH_GUILD_QUEST', 1, [['quest', 'int32']])
    mod.dispatch.addDefinition('C_GET_GUILD_QUEST_WEEKLY_REWARD', 1, [['index', 'int32']]);

    // Opcodes for patch 115
    // (those should already come with whatever toolbox you're using for v92 or v100)
    if (mod.publisher == 'gf' && mod.majorPatchVersion == 115) {
        mod.dispatch.addOpcode(`C_REQUEST_START_GUILD_QUEST`, 53117)
        mod.dispatch.addOpcode(`C_REQUEST_FINISH_GUILD_QUEST`, 35399)
        mod.dispatch.addOpcode(`C_REQUEST_CANCEL_GUILD_QUEST`, 30875)
        mod.dispatch.addOpcode(`S_GUILD_QUEST_LIST`, 43204)
    }

    // more opcodes that might be added... someday. If needed.
    /*
      if (mod.publisher == 'gf' && mod.majorPatchVersion == 115) {
      mod.dispatch.addOpcode(`S_GUILD_INFO`, 24743)
      mod.dispatch.addOpcode(`C_GET_GUILD_QUEST_WEEKLY_REWARD`, 31755)
      }
    */

    // don't remember the purpose of this but might be for retrieving quest names
    /*
      mod.game.on('enter_game', () => {
      mod.querydata('/strsheet_guildquest/string/', [], true).then(results => {

      })
      });
    */

    function getTime(){
        let date = new Date();
        let hours = ("0" + date.getHours()).slice(-2)
        let minutes = ("0" + date.getMinutes()).slice(-2)
        let currentTime = hours + ":" + minutes;

        return currentTime;
    }

    function message(msg){
        mod.command.message(msg);
        mod.log("[" + getTime() + "] " + msg.replace(/(<([^>]+)>)/ig,""));
    }

    function removeQuest(questList, questID){
        for (var i = 0; i < questList.length; i++){
            if (questList[i] === questID){
                questList.splice(i, 1);
                break;
            }
        }
    }

    function fakeLevelMessage() {
        mod.send('S_SYSTEM_MESSAGE', 1, {
            message: "@1108\u000bGuildLevel\u000b118"
        });
    }

    // retrieves guild quest name from datacenter. WIP! Should be called upon game start.
    /*
      async function newGetQuestName(questID){
      let query = await mod.queryData('/StrSheet_GuildQuest/String/', [], true, false, ['id', 'string']);
      query.forEach(result =>{
      questName[result.attributes.id] = result.attributes.string;
      });

      let questName = {};

      mod.log(questList[parseInt(questID + '001')])
      mod.log(questList)

      //return questName[questID]
      }
    */

    // hardcoded quest names.
    function getQuestName(questID){
        switch(questID) {
        case 10000: return "Victory in Battleground"; break;
        case 10001: return "Gathering"; break;
        case 10002: return "Hunting"; break;
        case 10003: return "Kill Boss Monsters"; break;
        case 10004: return "Fishing"; break;
        case 10005: return "Defeat Anansha"; break;
        case 10006: return "Defeat Frygaras"; break;
        case 10007: return "Defeat Sabranak"; break;
        case 10008: return "Defeat Anansha"; break;
        case 10009: return "Defeat Frygaras"; break;
        case 10010: return "Defeat Sabranak"; break;
        }
    }

    function sendFakeRallyMsg(type, zoneId, templateId, quest) {
        mod.send('S_NOTIFY_GUILD_QUEST_URGENT', 1, {
            type: type,
            zoneId: zoneId,
            templateId: templateId,
            quest: quest,
        });
        bossName = null;
        bossArea = null;
    }

    // Hooks

    /* Notifies in toolbox console when guild levels up. We're not using in-game
       chat since the game already does that with guild alerts. This is the SMT
       approach, which listens for the in-game message and then prints guild's level.

       "message": "@1108\u000bGuildLevel\u000b118"
    */
    mod.hook('S_SYSTEM_MESSAGE', 1, { filter: { fake: null } }, (event) => {
        if (!mod.settings.levelnotify) return;

        const msg = mod.parseSystemMessage(event.message);

        if (msg.id === 'SMT_CHANGE_GUILDLEVEL'){
            mod.log(`[${getTime()}] Guild Level is now ${msg.tokens.GuildLevel}.`);
        }
    });

    /* Warns about Guild BAM. Quest announcement should work anywhere, while
       spawn and death only works if user is in their respective area.
       "fake: null" is used here for both normal and test notification compatibility.
    */
    mod.hook('S_NOTIFY_GUILD_QUEST_URGENT', 1, { filter: { fake: null } }, (event) => {
        if (!mod.settings.gbamnotify) return;

        // set up boss name and their area based on quest's ID
        // event.quest = @GuildQuest:quest ID + 001
        if (!bossName) {
            switch(event.quest) {
            case '@GuildQuest:10005001':
                bossName = "Anansha";
                bossArea = "Windwhistle Woods, next to Tria (Essenia)";
                break;
            case '@GuildQuest:10006001':
                bossName = "Frygaras";
                bossArea = "Screaming Snowfield, next to Bleakrock (Westonia)";
                //bossZone = "34";
                //bossTemplate = "131203072";
                break;
            case '@GuildQuest:10007001':
                bossName = "Sabranak";
                bossArea = "Howling Glacier, south of Frost Reach (Westonia)";
                break;
            default:
                bossName = "Big Mean Mother Hubbard";
                bossArea = "somewhere";
                break;
            };
        }

        /* Quest availability, GBAM spawn and death notify. Message examples:
           [21:00] Rally quest for Frygaras in Screaming Snowfield, next to Bleakrock (Westonia) is now available.
           [18:09] Sabranak has appeared.
           [20:13] Anansha has been defeated.
        */
        switch(event.type) {
        case 0:
            message('Rally quest for <font color="#FE6F5E">' + bossName + '</font> in ' + bossArea + ' is now available.');
            break;
        case 1:
            message('<font color="#FE6F5E">' + bossName + '</font> has appeared.');
            break;
        case 3:
            message('<font color="#FE6F5E">' + bossName + '</font> has been defeated.');
            bossName = null;
            bossArea = null;
            break;
        };
    });


    /* This function loads the remaining hooks, which requires unmapped opcodes.
       There is an implementation which uses a unload() and hook() function, but
       don't think it will be necessary here since:

       - It won't load if failsafe was triggered
       - destructor() already unhooks everything
    */
    function load() {
        /* Users with 0 total contribution will be unable to automatically accept quests
           since I couldn't find another way of detecting accept permissions. Checking
           for Civil Unrest reward eligibility won't work because it requires the member
           to be in guild for a week, and the weekly cap could reset before it happens.

           Gets sent upon opening members tab or when logging a character.
        */
        mod.hook('S_GUILD_MEMBER_LIST', mod.majorPatchVersion >= 101 ? 2 : 1, (event) => {
            for (var i = 0; i < event.members.length; i++) {
                if (event.members[i].name === mod.game.me.name) {
                    // detects if current character is guild master
                    isGuildMaster = (event.guildMaster === mod.game.me.name);

                    // scuffed workaround for valderon trial
                    isNewbie = (event.members[i].contributionTotal == '0');
                    break;
                }
            }
        });

        /* Used for tracking active quests. In this case, we're using this for re-enabling
           them at midnight (03:00 UTC) since game cancels all of them for some reason and
           for avoiding quest re-enable.
           I could implement this within the hook above, but you know.. it wouldn't
           work with manually activated or completed quests. The (redundant) failsafe
           applies here as well. May automatically work or not, but yeah.. whatever.

           Gets sent upon opening quests tab, when logging a character or when a quest
           status (not progress, that's S_UPDATE_GUILD_QUEST_STATUS) changes.
        */
        mod.hook('S_GUILD_QUEST_LIST', 1, (event) => {
            if (!mod.settings.autoquest) return;
            if (mod.settings.gmonly && !isGuildMaster) return;

            // sets guild size based on quantity of accounts in guild
            if (event.size <= 39) {
                weeklyCap = "900"
            } else if (event.size >= 40 && event.size <= 79) {
                weeklyCap = "1300"
            } else {
                weeklyCap = "1700"
            }

            // warns user whenever a rally quest is up upon logging
            // untested, might not work!!!!!
            /*mod.game.on('enter_game', () => {
                for (var i = 0; i < event.quests.length; i++) {
                    if (event.quests[i].isRally == "2" && event.quests[i].status == "0") {
                        message(`There is a rally quest going on, please check guild quests!`)
                    };
                }
            });*/

            // add and remove quests from array
            for (var i = 0; i < event.quests.length; i++) {
                // add quests to array
                if (!activeQuests.includes(event.quests[i].id) && event.quests[i].status == "1") {
                    activeQuests.push(event.quests[i].id);
                    mod.log("(DEBUG) Quest " + event.quests[i].id + " added to activeQuests (quest list hook)");
                }
                // remove quests from array
                if (activeQuests.includes(event.quests[i].id) && event.quests[i].status == "0") {
                    removeQuest(activeQuests, event.quests[i].id);
                    mod.log("(DEBUG) Quest " + event.quests[i].id + " removed from activeQuests (quest list hook)");
                }
            }
        });

        /* Use those in case of fuck up
           mod.hook('C_REQUEST_START_GUILD_QUEST', 1, { filter: { fake: null } }, (event) => {
           if (failsafe || !mod.settings.autoquest) return;

           // add quest to array
           if (!activeQuests.includes(event.questId)) {
           activeQuests.push(event.questId);
           mod.log("(DEBUG) Quest " + event.questId + " added to activeQuests (C hook)");
           }
           });

           mod.hook('C_REQUEST_FINISH_GUILD_QUEST', 1, { filter: { fake: null } }, (event) => {
           if (failsafe || !mod.settings.autoquest) return;

           // remove quest from array
           if (activeQuests.includes(event.quest)) {
           removeQuest(activeQuests, event.quest)
           mod.log("(DEBUG) Quest " + event.quest + " removed from activeQuests (quest finished, C hook)");
           }
           });


           mod.hook('C_REQUEST_CANCEL_GUILD_QUEST', 1, { filter: { fake: null } }, (event) => {
           if (failsafe || !mod.settings.autoquest) return;

           // remove quest from array
           if (activeQuests.includes(event.questId)) {
           removeQuest(activeQuests, event.questId)
           mod.log("(DEBUG) Quest " + event.questId + " removed from activeQuests (quest cancelled, C hook)");
           }
           });
        */

        /* The original purpose of this mod. Awaits for full quest progess, then send a completion
           and a start request if unk1 equals to 2, but we're also checking current progress (total) and
           required points (completed) to make sure. Also (another) failsafe in case of game update.

           Gets sent every time a quest progress (kill a mob, catch a fish etc). In case of gathering
           (and possibly fishing), it gets sent multiple times, but I haven't figured out why.
           When a quest is ready to deliver, it sends an extra packet, which changes its state to
           completed.
           unk1 values seems to be the following: 1 for in progress, 2 for completed
           unk2 is remaining time in minutes

           event.maxQuests = total of valderon tokens (maybe calculate it if the above doesn't work)
        */
        mod.hook('S_UPDATE_GUILD_QUEST_STATUS', 1, (event) => {
            if (!mod.settings.autoquest) return;
            if (mod.settings.gmonly && !isGuildMaster) return;
            if (isNewbie) return;

            //mod.log("Count: " + event.targets[0].total + ", State: " + event.unk1 + ", Quest: " + getQuestName(event.quest));

            if (mod.game.me.alive && !mod.game.me.inBattleground) {
                // redundant check, i know!!! should check unk1 only.
                if (event.targets[0].total === event.targets[0].completed && event.unk1 == "2") {

                    //message("Quest " + getQuestName(event.quest) + " has state " + event.unk1 + " and ready to turn in");
                    /*
                      setTimeout(()=>{
                      mod.log("Finish quest");
                      }, 2000 + Math.random()*1000);

                      mod.log("Add quest to count");

                      setTimeout(() => {
                      mod.log("Accept quest");
                      }, 4000 + Math.random()*1000);
                    */

                    /*
                      setTimeout(()=>{
                      mod.send('C_REQUEST_FINISH_GUILD_QUEST', 1, {quest: event.quest})
                      }, 2000 + Math.random()*1000);
                    */

                    // finish quests and add to array
                    if (activeQuests.includes(event.quest)) {
                        setTimeout(()=>{
                            //mod.log("Finish quest");
                            mod.send('C_REQUEST_FINISH_GUILD_QUEST', 1, {quest: event.quest});
                            removeQuest(activeQuests, event.quest);
                            mod.log("(DEBUG) Quest " + event.quest + " completed and removed from activeQuests (update quest hook)");

                            // quest completion + total notifier
                            questCount++;
                            if (mod.settings.donenotify){
                                message("Quest " + getQuestName(event.quest) + " completed. Total: " + questCount);
                            }
                        }, 2000 + Math.random()*1000);
                    }

                    /* debug purposes only, remove later
                       if (rallyQuest.includes(event.quest)) mod.log("(DEBUG) Rally quest detected");
                       if (activeQuests.includes(event.quest)) mod.log("(DEBUG) Active quest detected: " + getQuestName(event.quest));
                       if (blockedQuests.includes(event.quest)) mod.log("(DEBUG) Blocked quest detected: " + getQuestName(event.quest));
                    */

                    //if (rallyQuest.includes(event.quest) || activeQuests.includes(event.quest) || blockedQuests.includes(event.quest)) return;
                    /* debug purposes only, remove later
                       if (rallyQuest.includes(event.quest)) mod.log("(DEBUG2) Rally quest not blocked from re-activation!");
                       if (activeQuests.includes(event.quest)) mod.log("(DEBUG2) Quest " + event.quest + " wasn't removed from array!");
                       if (blockedQuests.includes(event.quest)) mod.log("(DEBUG2) Blocked quest detected: " + event.quest);
                    */
                    /*
                      setTimeout(() => {
                      mod.send('C_REQUEST_START_GUILD_QUEST', 1, {questId: event.quest})
                      }, 4000 + Math.random()*1000);
                    */

                    // complete quest and add to array
                    setTimeout(() => {
                        if (rallyQuest.includes(event.quest)) {
                            mod.log("(DEBUG) Rally quest detected, avoiding activation")
                            return;
                        } else {
                            mod.log(`(DEBUG) ${event.quest} is not rally quest`)
                        }

                        //mod.log("Accept quest");
                        mod.send('C_REQUEST_START_GUILD_QUEST', 1, {questId: event.quest});
                        activeQuests.push(event.quest);
                        mod.log("(DEBUG) Quest " + event.quest + " started and added to activeQuests (update quest hook)");
                    }, 4000 + Math.random()*1000);
                }
            }
        });
    }

    // Load remaining functions if failsafe wasn't triggered
    if (!failsafe) load();

    // Commands
    mod.command.add('gq', {
	'$default': () => {
            mod.command.message("Usage:")
            mod.command.message("auto: Enables automatic quest accept and finish")
            mod.command.message("status: Print state of mod")
            mod.command.message("total: Total of completed quests since login")
            mod.command.message("watch: Watches for a specified event (run without arguments for usage)")
            mod.command.message("rtest: Sends a fake, client-sided rally quest notification")
            mod.command.message("gmonly: Makes quest manipulation functional for the guild master only")
            mod.command.message("ltest: Sends a fake, client-sided level up notification")
	},
        'auto': () => {
            if (!failsafe) {
	        mod.settings.autoquest = !mod.settings.autoquest
	        mod.command.message("Automatic handling of quests is now " + (mod.settings.autoquest ? "on" : "off") + ".");
                if (isNewbie) {
                    mod.command.message("Note: Your character is unable to accept and turn in quests until next valderon tokens reset.")
                }
            } else {
                mod.command.message('<font color="#FE6F5E">Cannot toggle auto mode: outdated mod</font>');
            }
	},
        'active': () => {
            if (activeQuests.length){
                let activeQuestsName = [];
                for ( var i = 0; i < activeQuests.length; i++ ){
                    activeQuestsName.push(getQuestName(activeQuests[i]))
                }
                mod.command.message("Current active quests: " + activeQuestsName);
            } else {
                mod.command.message("No active quests.");
            }
        },
        'total': () => {
            if (!failsafe) {
                mod.command.message("Automatically turned in " + questCount + " guild quests since login.");
            } else {
                mod.command.message('<font color="#FE6F5E">No quests were automatically turned in: mod is outdated</font>');
            }
        },
        'watch': (arg) => {
            switch(arg){
            case 'done': case 'complete': case 'finished':
                mod.settings.donenotify = !mod.settings.donenotify
	        mod.command.message("Completion notify is now " + (mod.settings.donenotify ? "on" : "off") + ".");
                if (failsafe) {
                    mod.command.message("Please note that this mode is meant for automatically completed quests, which is disabled until this mod gets updated.");
                }
                break;
            case 'gbam': case 'bam': case 'rally':
                mod.settings.gbamnotify = !mod.settings.gbamnotify;
	        mod.command.message("Guild BAM notify is now " + (mod.settings.gbamnotify ? "on" : "off") + ".");
                break;
            case 'level': case 'lvl':
                mod.settings.levelnotify = !mod.settings.levelnotify;
	        mod.command.message("Level up notify is now " + (mod.settings.levelnotify ? "on" : "off") + ".");
                break;
            default:
                mod.command.message("Possible arguments are:");
                mod.command.message("done|complete|finished: Watch for automatically completed quests.");
                mod.command.message("gbam|bam|rally: Watch for Guild BAM announcement, spawn and death (the last two only works in the BAM's province).");
                mod.command.message("level|lvl: Notifies guild level up in Toolbox's log.");
                break;
            }
        },
        'status': () => {
            mod.command.message("Failsafe: " + failsafe);
            if (!failsafe) {
                mod.command.message("Guild master: " + isGuildMaster);
                mod.command.message("Auto quest: " + (mod.settings.autoquest ? "on" : "off"));
                mod.command.message("Can activate quests: " + (isNewbie ? "no" : "yes"));
                mod.command.message("Guild master only: " + (mod.settings.gmonly ? "on" : "off"));
                mod.command.message("Auto completion notify: " + (mod.settings.donenotify ? "on" : "off"));
            }
            mod.command.message("Guild BAM notify: " + (mod.settings.gbamnotify ? "on" : "off"));
            mod.command.message("Level up notify: " + (mod.settings.levelnotify ? "on" : "off"));
        },
        /*'name': (arg) => {
          newGetQuestName();
          },*/
        'rtest': (arg) => {
            switch(arg){
            case 'Anansha': case 'anansha': case 'a':
                sendFakeRallyMsg("0", "29", "131203072", "@GuildQuest:10005001"); // wrong template but w/e
                break;
            case 'Frygaras': case 'frygaras': case 'f':
                sendFakeRallyMsg("0", "34", "131203072", "@GuildQuest:10006001");
                break;
            case 'Sabranak': case 'sabranak': case 's':
                sendFakeRallyMsg("0", "34", "131203072", "@GuildQuest:10007001"); // same
                break;
            case 'None': case 'none': case 'n':
                sendFakeRallyMsg("0", "0", "0", "@GuildQuest:10009999"); // same
                break;
            default:
                mod.command.message("Possible arguments are: Anansha, Frygaras, Sabranak and none.");
                break;
            }
        },
        'ltest': () => {
            fakeLevelMessage();
        },
        'gmonly': () => {
            if (!failsafe) {
	        mod.settings.gmonly = !mod.settings.gmonly
	        mod.command.message("Guild master only is now " + (mod.settings.gmonly ? "on" : "off") + ".");
            } else {
                mod.command.message('<font color="#FE6F5E">Cannot toggle mode: mod is outdated</font>');
            }


        }
    });

    // required for reloading
    this.destructor = () => {
        mod.command.remove('gq');
    }
    this.saveState = () => {
        mod.log('user called reload, saving mod state');
        return {
            supportedVersion: supportedVersion,
            failsafe: failsafe,
            isNewbie: isNewbie,
            isGuildMaster: isGuildMaster,
            questCount: questCount,
            guildLevel: guildLevel,
            weeklyCap: weeklyCap,
            bossName: bossName,
            bossArea: bossArea,
            activeQuests: activeQuests
        };
    }
    this.loadState = (state) => {
        mod.log('loading state');
        supportedVersion = state.supportedVersion;
        failsafe = state.failsafe;
        isNewbie = state.isNewbie;
        isGuildMaster = state.isGuildMaster;
        questCount = state.questCount;
        guildLevel = state.guildLevel;
        weeklyCap = state.weeklyCap;
        bossName = state.bossName;
        bossArea = state.bossArea;
        activeQuests = state.activeQuests;
    }
}
