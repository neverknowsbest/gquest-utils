module.exports = [
    {
        "key": "autoquest",
        "name": "Guild Quest",
        "type": "bool"
    },
    {
        "key": "donenotify",
        "name": "Completed quests notify",
        "type": "bool"
    },
    {
        "key": "levelnotify",
        "name": "Guild level-up notify",
        "type": "bool"
    },
    {
        "key": "gbamnotify",
        "name": "Rally quest notify",
        "type": "bool"
    }
    {
        "key": "gmonly",
        "name": "Guild Master only",
        "type": "bool"
    }
];
