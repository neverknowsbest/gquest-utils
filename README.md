## gquest-utils
Automatically accept (and turn-in) guild quests, recieve rally quests notifications and much more.

**NOTE:** This is a personal mod of mine, there are probably code flaws and other stuff I can't be bothered to clean up.

# Compatibility
- 115.2
- 100.2
- 92.4

# Installation
You can either:
  - Click the download button next to "Clone", then select any format (usually .zip) and extract it inside `<TeraToolbox>/mods`
  - Create a folder called `gquest-utils` inside `<TeraToolbox/mods>`, then place [`modules.json`](https://gitlab.com/neverknowsbest/gquest-utils/-/raw/master/module.json) inside and start Toolbox
  - If you have git installed, run `git clone https://gitlab.com/neverknowsbest/gquest-utils` inside `<TeraToolbox>/mods`

## Commands
Use after `/8` or `/toolbox` or in any chat with the "!" prefix (e.g !gq).

| Command                                       | description                                                             |
|-----------------------------------------------|-------------------------------------------------------------------------|
| **gq**                                        | Module on/off.                                                          |
| **gq auto**                                   | Enable automatic accept and completion of quests.                       |
| **gq status**                                 | Print status of mod.                                                    |
| **gq total**                                  | Total of completed quests since login.                                  |
| **gq watch (done/gbam/level)**                | Toggle notification for quest completion, guild bam or guild level up.  |
| **gq rtest (anansha/frygaras/sabranak/none)** | Sends a fake, client-sided rally quest notification.                    |
| **gq gmonly**                                 | Makes the quest completion functional only if you are the guild master. |
| **gq ltest**                                  | Sends a fake, client-sided guild level-up message.                      |

## Known issues:
* On retail, quests would automatically be disabled after a certain hour. This came from the server itself, but the module is not able to re-activate them when it happens.
* Mod cannot determine if your current character is able to accept, complete or cancel quests.
* Hardcoded data. This can be a problem if, let's say, you're playing in a patch before quests got changed or private server operators decide to create/edit them.

## TODO:
* Clean up (useless functions, comments and other stuff)
* Remove hardcoded quest IDs and names, retrieve them directly from datacenter
* Re-enable quests that were disabled automatically after a certain hour (usually at 5 AM CEST in TERA EU)
* Prevent accidental re-activation of gray quests
* Better way of determining if character is valderon newbie (quest activation and box claim)

## Credits:
* PsykoDev: Original guild quest module
* Hailstorming (and contributors): For creating [baldera-reborn](https://github.com/hailstorming/Baldera-Reborn), which I used as inspiration for certain stuff (adding definitions and opcodes directly from the mod, failsafe, code commenting and maybe other stuff)